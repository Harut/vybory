from webob import Request

class GuardedRequest(Request):
    cookies = None
    GET = None
    headers = None
    # XXX raise attribute error in place of None?

    def __init__(self, request):
        d = dict(request.__dict__,
                 _request = request,
                 headers = None)
        self.__dict__.update(d)

    def __setattr__(self, key, value):
        raise AttributeError(u'setattr is disabled for guarded request')

    def unwrap(self):
        return self._request
