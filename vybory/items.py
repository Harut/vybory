# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class IzbirkomItem(scrapy.Item):

    id = scrapy.Field()
    uid = scrapy.Field() # all parents' names
    name = scrapy.Field()
    parent_id = scrapy.Field({'fk': 'izbirkom',
                              'uid': lambda x: x['uid'].rsplit('\\', 1)[0] if '\\' in x['uid'] else None })


class ResultTypeItem(scrapy.Item):

    id = scrapy.Field()
    is_candidate = scrapy.Field()
    name = scrapy.Field()


class VyboryItem(scrapy.Item):

    id = scrapy.Field()
    vrn = scrapy.Field() # uid
    title = scrapy.Field()
    campaign_start = scrapy.Field()
    vote_day = scrapy.Field()
    izbirkom_uid = scrapy.Field()
    izbirkom_id = scrapy.Field({'fk': 'izbirkom',
                                'uid': lambda x: x['izbirkom_uid'] })


class VotingItem(scrapy.Item):

    id = scrapy.Field()
    vrn = scrapy.Field() # vybory uid
    tvd = scrapy.Field() # voting uid (???)
    candidates_uid = scrapy.Field() # voting uid (???)
    parent_tvd = scrapy.Field()
    parent_id = scrapy.Field({'fk': 'voting',
                              'uid': lambda x: (x['vrn'], x['parent_tvd'], x['candidates_uid']) if x['parent_tvd'] is not None else None})
    izbirkom_uid = scrapy.Field()
    izbirkom_id = scrapy.Field({'fk': 'izbirkom',
                                'uid': lambda x: x['izbirkom_uid'] })


class ResultItem(scrapy.Item):

    id = scrapy.Field()
    vrn = scrapy.Field() # vybory/voting uid
    tvd = scrapy.Field() # voting uid
    candidates_uid = scrapy.Field() # voting uid
    is_candidate = scrapy.Field()

    name = scrapy.Field()
    value = scrapy.Field()
    percent = scrapy.Field()

    voting_id = scrapy.Field({'fk': 'voting',
                              'uid': lambda x: (x['vrn'], x['tvd'], x['candidates_uid'])})
    result_type_id = scrapy.Field({'fk': 'resulttype',
                                   'uid': lambda x: (x['name'].upper(), x['is_candidate'])})


