# -*- coding: utf-8 -*-
from __future__ import absolute_import

from hashlib import md5
import re
import logging
#logging.basicConfig()
if 0:
    import colordebug
    color_formatter = colordebug.ColoredFormatter()
    logging.root.handlers[0].setFormatter(color_formatter)
import scrapy
from functools import partial

from vybory.items import VyboryItem, VotingItem, ResultItem, ResultTypeItem, IzbirkomItem
from front import models

logger = logging.getLogger(__name__)


def int_url_param(name, url):
    match = re.search(r'(?:&|\?)'+name+'=(\d+)', url)
    if match is not None:
        return int(match.groups()[0])


# XXX referendums does not match!
# XXX vibory.izbirkom is not filled


HARDCODED = {
    265200067619: {
        'campaign_start': '02.09.2003',
        'vote_day': '07.12.2003',
        'izbirkom_uid': u'Избирательная комиссия Сахалинской области',
    },
    265200073322: {
        'campaign_start': '02.09.2003',
        'vote_day': '21.12.2003',
        'izbirkom_uid': u'Избирательная комиссия Сахалинской области',
    }
}

fix_commission_name = models.Izbirkom.normalize_name


class VyboryRegionalSpider(scrapy.Spider):

    name = 'vybory'
    REGIONAL_URL = 'http://izbirkom.ru/izbirkom/calendar/?mode=result&date=29.11.2003&date2=13.09.2075&regions_code%5B%5D=65&sxemavib%5B%5D=all&urovproved%5B%5D=all&vidvibref%5B%5D=all&vibtype%5B%5D=all'
    FEDERAL_URL = 'http://izbirkom.ru/izbirkom/calendar/?mode=result&date=29.11.2003&date2=13.09.2075&regions_code%5B%5D=0&sxemavib%5B%5D=all&urovproved%5B%5D=1&vidvibref%5B%5D=all&vibtype%5B%5D=all'
    start_urls = [REGIONAL_URL, FEDERAL_URL]

    def parse(self, response):
        for link in response.css('#result_table .ajax_reg a'):
            url = response.urljoin(link.css("::attr('href')").extract()[0])
            vybory = VyboryItem()
            vybory['title'] = link.xpath('text()').extract()[0]
            vybory['vrn'] = int_url_param('vrn', url)

            start_url = response.request.url

            if start_url == self.REGIONAL_URL:
                # regional cik
                callback = partial(self.parse_regional_cik_page, vybory=vybory)
            else:
                # federal cik
                callback = partial(self.parse_cik_page, vybory=vybory)
            yield scrapy.Request(url, callback)

    def parse_commission(self, response):
        def table_cell(title):
            xpath = ur'//tr[td/b/text()="{}"]/td[not(b)]/text()'.format(title)
            return response.xpath(xpath).extract_first()
        BC_XPATH = "//table/tr/td[@align='center'][contains(text(), '>')]/a/text()"
        breadcrumbs = response.xpath(BC_XPATH)
        commission = (breadcrumbs and breadcrumbs[-1].extract()) or \
                     table_cell(u"Наименование комиссии") or \
                     table_cell(u"Наименование Избирательной комиссии")
        commission = fix_commission_name(commission)
        return commission


    def parse_vybory(self, response, vybory):
        def table_cell(title):
            xpath = ur'//tr[td/b/text()="{}"]/td[not(b)]/text()'.format(title)
            return response.xpath(xpath).extract_first()
        commission = self.parse_commission(response)

        vybory['izbirkom_uid'] = commission
        vybory['campaign_start'] = table_cell(u"Дата объявления начала избирательной кампании") or \
                                   table_cell(u"Дата объявления")
        vybory['vote_day'] = table_cell(u"Дата голосования")

        if vybory['vrn'] in HARDCODED:
            logger.info('HARDCODED vybory %s', vybory['vrn'])
            vybory.update(HARDCODED[vybory['vrn']])


        return vybory

    def parse_cik_page(self, response, vybory=None):
        vybory = self.parse_vybory(response, vybory)

        izbirkom = IzbirkomItem()
        izbirkom['name'] = vybory['izbirkom_uid'].rsplit('\\', 1)[-1]
        izbirkom['uid'] = vybory['izbirkom_uid']
        yield izbirkom
        yield vybory

        regional_url = response.xpath(u"//select[@name = 'gs']/option[contains(text(), 'Сахалин')]/@value").extract_first()
        if regional_url:
            callback = partial(self.parse_regional_cik_page, vybory=vybory)
            yield scrapy.Request(regional_url, callback)

    def parse_regional_cik_page(self, response, vybory=None, parent_voting=None):
        REGIONAL_PAGE_TITLE = u'сайт избирательной комиссии субъекта Российской Федерации'
        regional_page = response.xpath(u'//a[text() = "{}"]/@href'.format(REGIONAL_PAGE_TITLE))\
                                .extract_first()
        if regional_page:
            callback = partial(self.parse_regional_cik_page, vybory=vybory, parent_voting=parent_voting)
            yield scrapy.Request(regional_page, callback)
        else:
            commission = self.parse_commission(response)

            yield_vybory = False
            if not vybory.get('campaign_start'):
                vybory = self.parse_vybory(response, vybory)
                yield_vybory = True

            if not commission:
                if parent_voting:
                    commission = parent_voting['izbirkom_uid'].split('\\')[-1]
                else:
                    commission = vybory['izbirkom_uid'].split('\\')[-1]

            tvd = int_url_param('tvd', response.request.url)



            if parent_voting and parent_voting['tvd'] == tvd:
                voting = parent_voting
            else:
                parent_uid = parent_voting['izbirkom_uid'] if parent_voting else vybory['izbirkom_uid']
                if parent_uid.split('\\')[-1] == commission:
                    izbirkom_uid = parent_uid
                elif parent_uid.split('\\')[-1].startswith(u'ОИК '):
                    izbirkom_uid = '\\'.join(parent_uid.split('\\')[:-1] + [commission])
                else:
                    izbirkom_uid = '\\'.join([parent_uid, commission])

                voting = VotingItem()
                voting['vrn'] = vybory['vrn']
                voting['tvd'] = int_url_param('tvd', response.request.url)
                voting['parent_tvd'] = parent_voting['tvd'] if parent_voting else None
                voting['izbirkom_uid'] = izbirkom_uid
                #yield voting

                izbirkom = IzbirkomItem()
                izbirkom['name'] = commission
                izbirkom['uid'] = izbirkom_uid
                #logger.info('Yield izbirkom "%s" from %s', izbirkom['name'], response.url)
                yield izbirkom

            if yield_vybory:
                yield vybory

            children = response.xpath("//select[@name = 'gs']/option/@value").extract()
            for child in children:
                callback = partial(self.parse_regional_cik_page, vybory=vybory, parent_voting=voting)
                yield scrapy.Request(child, callback)

            results = response.xpath(u'//a[starts-with(text(), "Итоги") or '
                                         u'starts-with(text(), "Результат")]/@href').extract()
            for result in results:
                callback = partial(self.parse_results, vybory=vybory, voting=voting)
                yield scrapy.Request(result, callback)

    def parse_results(self, response, vybory=None, voting=None):
        rows = response.xpath(u'//table[tr/td[starts-with(text(), "Число")]]/tr')
        is_candidate = False
        candidates = []
        results = []
        for row in rows:
            cells = row.css('td')
            if len(cells) != 3:
                #logger.debug('SKIP ROW %s', row.extract())
                is_candidate = True
                continue
            result = ResultItem()
            result['vrn'] = vybory['vrn']
            result['tvd'] = voting['tvd']
            result['name'] = ''.join(cells[1].xpath(".//text()").extract()).strip()
            result['is_candidate'] = is_candidate and not result['name'].upper().startswith(u'ЧИСЛО ')
            #logger.debug('Extracting result for %s is_candidate=%s', result['name'], result['is_candidate'])
            result['value'] = int(cells[2].xpath("b/text()").extract_first().strip())
            result['percent'] = cells[2].xpath('./text()').re_first('[\.\d]+%')
            result['percent'] = (result['percent'] or '').rstrip('%')
            if is_candidate:
                candidates.append(result['name'])
            results.append(result)

            result_type = ResultTypeItem()
            result_type['is_candidate'] = result['is_candidate']
            result_type['name'] = result['name']
            yield result_type

        candidates_uid = md5(';'.join(candidates).encode('utf-8')).hexdigest()[:16]
        voting = voting.copy()
        voting['candidates_uid'] = candidates_uid
        yield voting
        for result in results:
            result['candidates_uid'] = candidates_uid
            yield result

