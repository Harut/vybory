# -*- coding: utf-8 -*-
import re
from .base import FrontModel
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Date, \
        BigInteger
from sqlalchemy.orm import relationship, foreign


__all__ = ['Izbirkom', 'ResultType', 'Vybory', 'Voting', 'Result']


REGION_ID = 65


IZBIRKOM_ALIASES = {
    u'Сахалинская область': u'Избирательная комиссия Сахалинской области',
}

class Izbirkom(FrontModel):

    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey(id))
    parent = relationship('Izbirkom', remote_side=id)
    children = relationship('Izbirkom',
                            remote_side=parent_id,
                            #order_by='TextPage.order'
                            )
    name = Column(String(250))

    @property
    def short_name(self):
        return self.name.replace('  ', ' ').replace(u'территориальная избирательная комиссия', u'ТИК')

    @staticmethod
    def normalize_name(name):
        name = re.sub('\s+', ' ', name or '').strip()
        name = name.replace(u'№ ', u'№')
        name = re.sub(u" территориальная избирательная комиссия$", "",
                            name, flags=re.I | re.U)
        name = re.sub(u" ТИК$", "", name, flags=re.I | re.U)
        name = re.sub(u"^УИК (\d+)$", ur'УИК №\1', name, flags=re.I | re.U)
        name = IZBIRKOM_ALIASES.get(name, name)
        return name

    @property
    def normalized_name(self):
        return self.normalize_name(self.name)

    @property
    def path(self):
        path = ''
        if self.parent:
            path += self.parent.path + ' \\ '
        return path + self.name


class ResultType(FrontModel):

    id = Column(Integer, primary_key=True)
    is_candidate = Column(Boolean, nullable=False, default=False)
    name = Column(String(250), nullable=True, default='')

    @staticmethod
    def normalize_name(name):
        name = re.sub('\s+', ' ', name.strip())
        name = name.replace(u'"', '')
        name = name.replace(u'«', '')
        name = name.replace(u'»', '')
        name = re.sub('^\d+\.', '', name, flags=re.I | re.U).strip()
        name = re.sub(u'^Местное отделение Партии (.+) муниципального образования .+', r'\1', name, flags=re.I | re.U)
        name = re.sub(u'.*местное отделение партии ', '', name, flags=re.I | re.U)
        name = re.sub(u'.*местное отделение политической партии ', '', name, flags=re.I | re.U)
        name = re.sub(u'.*местное отделение ', '', name, flags=re.I | re.U)
        name = re.sub(u'^\w+ое отделение(:?(:?(:? всероссийской)? политической)? партии)? ', '', name, flags=re.I | re.U)
        name = re.sub(u'^\w+ое областное отделение(:?(:?(:? всероссийской)? политической)? партии)? ', '', name, flags=re.I | re.U)
        name = re.sub(u'^\w+о(:?е|го) регионально(:?е|го) отделени[яе](:? партии)?(:? политической партии)? ', '', name, flags=re.I | re.U)
        name = re.sub(u'^всероссийск(:?ая|ой) политическ(:?ая|ой) парт(:?ия|ии) ', '', name, flags=re.I | re.U)
        name = re.sub(u'^РО во? [\w -]+ Политической партии ', '', name, flags=re.I | re.U)
        name = re.sub(u'^РО(?: ПП) (.+) в [\w -]+ области', r'\1', name, flags=re.I | re.U)
        name = re.sub(u'^политическая партия ', r'', name, flags=re.I | re.U)
        name = re.sub(u' в \w+ой области$', r'', name, flags=re.I | re.U)
        name = re.sub(u',? \w+ое региональное отделение$', r'', name, flags=re.I | re.U)
        return name

    @property
    def normalized_name(self):
        return self.normalize_name(self.name)



class Vybory(FrontModel):

    id = Column(Integer, primary_key=True)
    vrn = Column(BigInteger, unique=True)
    title = Column(String(1000))
    campaign_start = Column(Date)
    vote_day = Column(Date)
    izbirkom_id = Column(Integer, ForeignKey(Izbirkom.id))
    izbirkom = relationship(Izbirkom)

    @property
    def url(self):
        BASE = 'http://www.vybory.izbirkom.ru/region/izbirkom/?action=show&region={}&vrn={}'
        return BASE.format(REGION_ID, self.vrn)

    __mapper_args__ = {'order_by': [vote_day.desc()]}


class Voting(FrontModel):

    id = Column(Integer, primary_key=True)
    vrn = Column(BigInteger)
    tvd = Column(BigInteger)
    candidates_uid = Column(String(16))

    parent_tvd = Column(BigInteger)
    parent_id = Column(Integer, ForeignKey(id))
    parent = relationship('Voting', remote_side=id)
    children = relationship('Voting',
                            remote_side=parent_id,
                            )

    izbirkom_id = Column(Integer, ForeignKey(Izbirkom.id))
    izbirkom = relationship(Izbirkom)
    vybory = relationship(Vybory, primaryjoin=foreign(Vybory.vrn)==vrn, uselist=False)

    @property
    def url(self):
        BASE = 'http://www.vybory.izbirkom.ru/region/izbirkom/?action=show&region={}&vrn={}&tvd={}'
        return BASE.format(REGION_ID, self.vrn, self.tvd)


class Result(FrontModel):

    id = Column(Integer, primary_key=True)
    vrn = Column(BigInteger)
    tvd = Column(BigInteger)
    candidates_uid = Column(String(16))

    voting_id = Column(Integer, ForeignKey(Voting.id))
    voting = relationship(Voting)

    result_type_id = Column(Integer, ForeignKey(ResultType.id))
    result_type = relationship(ResultType)

    value = Column(Integer)
    percent = Column(String(10))
