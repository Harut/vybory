# -*- coding: utf-8 -*-

from sqlalchemy.schema import MetaData
from sqlalchemy.ext.declarative import declarative_base
from iktomi.db.sqla.declarative import AutoTableNameMeta, TableArgsMeta


TABLE_ARGS = {
    'mysql_engine': 'InnoDB',
    'mysql_default charset': 'utf8',
}


class ModelsMeta(AutoTableNameMeta, TableArgsMeta(TABLE_ARGS)):

    pass


metadata = MetaData()

FrontModel = declarative_base(metadata=metadata,
                              name='FrontModel',
                              metaclass=ModelsMeta)


