from front.models import Vybory, Voting, Result, ResultType, Izbirkom
from sqlalchemy import func
from webob.exc import HTTPNotFound

class Or404(object):
    def __ror__(self, arg):
        if not arg:
            raise HTTPNotFound()
        return arg
    __or__ = __ror__
_404 = Or404()


def index(env, data):
    vyborys = env.db.query(Vybory).all()

    vybory_count = env.db.query(func.count('*')).select_from(Vybory).scalar()
    voting_count = env.db.query(func.count('*')).select_from(Voting).scalar()
    results_count = env.db.query(func.count('*')).select_from(Result).scalar()
    izbirkoms_count = env.db.query(func.count('*')).select_from(Izbirkom).scalar()
    result_types_count = env.db.query(func.count('*'))\
                               .select_from(ResultType)\
                               .filter_by(is_candidate=False)\
                               .scalar()
    candidates_count = env.db.query(func.count('*'))\
                             .select_from(ResultType)\
                             .filter_by(is_candidate=True)\
                             .scalar()
    return env.render_to_response('index', dict(
        vyborys=vyborys,
        vybory_count = vybory_count,
        voting_count = voting_count,
        izbirkoms_count=izbirkoms_count,
        results_count = results_count,
        result_types_count = result_types_count,
        candidates_count = candidates_count,
    ))


def types(env, data):
    result_types = env.db.query(ResultType)\
                         .filter_by(is_candidate=False)\
                         .all()
    ids = [x.id for x in result_types]
    counts = env.db.query(Result.result_type_id, func.count('*')) \
                   .filter(Result.result_type_id.in_(ids))\
                   .group_by(Result.result_type_id)\
                   .all()
    return env.render_to_response('types', dict(
        result_types=result_types,
        counts=dict(counts),
    ))


def types_item(env, data):
    item = env.db.query(ResultType)\
                 .filter_by(is_candidate=False,
                            id=data.id)\
                 .first()
    return env.render_to_response('types_item', dict(
        item=item,
    ))


def candidates(env, data):
    candidates = env.db.query(ResultType)\
                       .filter_by(is_candidate=True)\
                       .order_by(ResultType.name)\
                       .all()
    return env.render_to_response('candidates', dict(
        candidates=candidates,
    ))


def candidates_item(env, data):
    item = env.db.query(ResultType)\
                 .filter_by(is_candidate=True,
                            id=data.id)\
                 .first()
    return env.render_to_response('candidates_item', dict(
        item=item,
    ))


def commissions(env, data):
    commissions = env.db.query(Izbirkom).order_by(Izbirkom.name).all()
    return env.render_to_response('commissions', dict(
        commissions=commissions,
    ))

def commissions_item(env, data):
    item = _404 | env.db.query(Izbirkom)\
                        .filter_by(id=data.id)\
                        .first()
    children = sorted(item.children, key=lambda x: x.name)

    votings = env.db.query(Voting)\
                    .filter_by(izbirkom=item)\
                    .all()

    return env.render_to_response('commissions_item', dict(
        item=item,
        children=children,
        votings=votings,
    ))


def vybory(env, data):
    vybory = _404 | env.db.query(Vybory)\
                          .filter_by(vrn=data.vrn)\
                          .first()
    votings = env.db.query(Voting)\
                    .filter_by(parent=None, vrn=data.vrn)\
                    .all()
    return env.render_to_response('vybory', dict(
        vybory=vybory,
        votings=votings
    ))



def voting(env, data):
    vybory = _404 | env.db.query(Vybory)\
                          .filter_by(vrn=data.vrn)\
                          .first()
    izbirkom = _404 | env.db.query(Izbirkom)\
                            .filter_by(id=data.izbirkom_id)\
                            .first()

    voting = _404 | env.db.query(Voting) \
                          .filter_by(izbirkom=izbirkom,
                                     vrn=data.vrn,
                                     candidates_uid=data.candidates_uid)\
                          .first()

    children = [x for x in voting.children
                if x.candidates_uid==voting.candidates_uid]
    orphans = [x for x in voting.children
               if x.candidates_uid!=voting.candidates_uid]

    voting_ids = [x.id for x in [voting] + children]
    results = env.db.query(Result)\
                    .filter(Result.voting_id.in_(voting_ids)) \
                    .all()

    result_types = []
    candidates = []
    seen_types = set()
    result_grid = {}
    for result in results:
        if result.result_type not in seen_types:
            if result.result_type.is_candidate:
                candidates.append(result.result_type)
            else:
                result_types.append(result.result_type)
            seen_types.add(result.result_type)
        result_grid[(result.voting, result.result_type)] = result

    #def percent(voting, candidate):
    #    # XXX total is not a sum of all candidates
    #    total = sum(result_grid[(voting, x)].value
    #                for x in candidates)
    #    percent = 100.0 * result_grid[(voting, candidate)].value / total
    #    return "{:.02f}".format(percent)

    return env.render_to_response('voting', dict(
        vybory=vybory,
        izbirkom=izbirkom,
        voting=voting,
        children=children,
        orphans=orphans,
        result_types=result_types,
        candidates=candidates,
        result_grid=result_grid,
        #percent=percent,
        ))
