# -*- coding: utf-8 -*-
import os
import hashlib
import json
import jinja2
from jinja2 import Markup
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from iktomi.templates import Template, BoundTemplate as BaseBoundTemplate
from iktomi.templates.jinja2 import TemplateEngine
from iktomi import web
from iktomi.web.filters import static_files
from iktomi.utils.storage import storage_method, storage_property, \
                                 storage_cached_property
from iktomi.utils import cached_property
from webob.exc import HTTPSeeOther

import cfg

static = static_files(cfg.STATIC, cfg.STATIC_URL)
static_reverse = static.construct_reverse()
_static_hashes = {}


def md5sum(filename, blocksize=65536):
    # XXX s this ok?
    if filename not in _static_hashes:
        hash = hashlib.md5()
        with open(filename) as f:
            for block in iter(lambda: f.read(blocksize), ""):
                hash.update(block)
        _static_hashes[filename] = hash.hexdigest()
    return _static_hashes[filename]


class BoundTemplate(BaseBoundTemplate):

    #constant_template_vars = dict(template_functions,
    #        **dict([(key, getattr(cfg, key))
    #               for key in cfg.TEMPLATE_IMPORT_SETTINGS]))

    def get_template_vars(self):
        d = dict(
            url = self.env.root,
            url_for = self.env.url_for,
            url_for_static = self.env.url_for_static,
            now = datetime.now(),
        )
        return d


class FrontTemplateEngine(TemplateEngine):

    def _make_env(self, paths):
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(paths),
            autoescape=True,
            extensions=[
                'jinja2.ext.i18n',
                'jinja2.ext.loopcontrols',
                'jinja2.ext.with_',
                #'jinja2htmlcompress.SelectiveHTMLCompress',
                #'jinja2htmlcompress.HTMLCompress',
            ],
        )
        env.extend(cfg=cfg)
        # Null translations in globals are replaced (actually masked with
        # locals) returned by BoundTemplate.get_template_vars().
        #env.globals = dict(env.globals,
        #                   **BoundTemplate.constant_template_vars)
        return env




db_maker = sessionmaker()
engine = create_engine(cfg.DATABASE, **cfg.DATABASE_PARAMS)
db_maker.configure(bind=engine)
jinja_loader = FrontTemplateEngine(cfg.TEMPLATES)
template_loader = Template(engines={'html': jinja_loader, 'json': jinja_loader},
                           *cfg.TEMPLATES)

#jinja_loader.env.filters['filesizeformat'] = do_filesizeformat_i18n
#jinja_loader.env.filters['typograph'] = do_typograph


class FrontEnvironment(web.AppEnvironment):
    cfg = cfg

    def __init__(self, *args, **kwargs):
        super(FrontEnvironment, self).__init__(*args, **kwargs)
        #self.template_data = dict(
        #    # env object is self._root_storage, self is just StorageFrame
        #    context = Context(self._root_storage))

    @storage_method
    def url_for_static(self, path, add_sum=True):
        if add_sum:
            sum = md5sum(os.path.join(self.cfg.STATIC, path))[:10]
            # XXX
            return self.cfg.STATIC_URL + path + '?' + sum
        return self.cfg.STATIC_URL + path

    @cached_property
    def url_for(self):
        return self.root.build_url

    @storage_cached_property
    def template(storage):
        return BoundTemplate(storage, template_loader)

    @storage_method
    def render_to_string(storage, template_name, _data, *args, **kwargs):
        _data = dict(storage.template_data, **_data)
        result = storage.template.render(template_name, _data, *args, **kwargs)
        return Markup(result)

    @storage_method
    def render_to_response(self, template_name, _data,
                           content_type="text/html"):
        #_data = dict(self.template_data, **_data)
        return self.template.render_to_response(template_name, _data,
                                                content_type=content_type)

    @storage_method
    def redirect_to(storage, name, qs, **kwargs):
        url = storage.url_for(name, **kwargs)
        if qs:
            url = url.qs_set(qs)
        return HTTPSeeOther(location=str(url))

    def json(self, data):
        #encoded = json.dumps(data, ensure_ascii=False)
        encoded = json.dumps(data)
        return web.Response(encoded, content_type="application/json")

    @cached_property
    def db(self):
        return db_maker()


@web.request_filter
def environment(env, data, next_handler):
    old_mask = os.umask(002)
    env.data = data # XXX hack!
    try:
        return next_handler(env, data)
    finally:
        os.umask(old_mask)
        env.db.close()



