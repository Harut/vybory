# -*- coding: utf-8 -*-


from os import path
import sys
import os

ROOT = os.path.dirname(os.path.join(os.path.abspath(__file__), '..'))


def path_config():
    for path in [ROOT]:
        if path not in sys.path:
            sys.path.insert(0, path)

path_config()


LOG_DIR = os.path.join(ROOT, 'logs')
RUN_DIR = os.path.join(ROOT, 'run')

SERVER_ERROR_TIMEOUT = 60
EMAIL_ERRORS_TO = ['genotip7@gmail.com']
LOG_FORMAT = '[%(levelname)s] %(name)-20s: %(message)s (%(name)-6s, %(filename)s:%(lineno)d), %(asctime)s'

SERVE_STATIC = False

SITE_DIR = path.dirname(path.abspath(__file__))

TEMPLATES = [
    path.join(SITE_DIR, 'templates'),
]

STATIC = path.join(SITE_DIR, 'static')
STATIC_URL = '/static/'

TEMPLATE_IMPORT_SETTINGS = ['STATIC_URL', 'MEDIA_URL']


DATABASE = 'mysql://root:@localhost/vybory?charset=utf8'

DATABASE_PARAMS = {
    'pool_size': 10,
    'max_overflow': 50,
    'pool_recycle': 3600,
}

RAW_JS = RAW_CSS = False

SMTP_HOST="localhost"
SMTP_PORT=25
SMTP_CHARSET="utf-8"
SMTP_FROM="noreply@vybory.local"

CACHE_PAGES_ENABLED = True
DEFAULT_CACHE_DURATION = 60*5


# Do not change defaults, overwrite params in FASTCGI_PARAMS instead
FASTCGI_PREFORKED_DEFAULTS = dict(
    preforked=True,
    multiplexed=False,
    minSpare=1,
    maxSpare=5,
    maxChildren=50,
    maxRequests=0,
)

# Do not change defaults, overwrite params in FASTCGI_PARAMS instead
FASTCGI_THREADED_DEFAULTS = dict(
    preforked=False,
    multithreaded=True,
    multiprocess=False,
    multiplexed=False,
    minSpare=1,
    maxSpare=5,
    maxThreads=sys.maxint,
)

FASTCGI_PARAMS = dict(
    FASTCGI_PREFORKED_DEFAULTS,
    maxSpare=8,
    minSpare=8,
    maxChildren=2,
)

FLUP_ARGS = dict(
    fastcgi_params = FASTCGI_PARAMS,
    umask = 0,
    bind = path.join(RUN_DIR, 'front.sock'),
    pidfile = path.join(RUN_DIR, 'front.pid'),
    logfile = path.join(LOG_DIR, 'front.log'),
)

cfg_local_file = path.join(SITE_DIR, 'cfg_local.py')
if path.isfile(cfg_local_file):
    execfile(cfg_local_file)
