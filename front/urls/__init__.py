# -*- coding: utf-8 -*-

from webob.exc import HTTPNotFound, HTTPMovedPermanently
from iktomi import web
from front import cfg

from front.environment import environment, static
from .filters import Rule, cache


from front import views

__all__ = ['app']



main_app = web.cases(
    Rule('/', views.index),
    Rule('/election/<int:vrn>', views.vybory),
    Rule('/election/<int:vrn>/<candidates_uid>/ik/<int:izbirkom_id>', views.voting),

    web.prefix("/candidates", name="candidates") | web.cases(
        web.match("") | views.candidates,
        web.match("/<int:id>", 'item') | views.candidates_item,
    ),

    web.prefix("/types", name="types") | web.cases(
        web.match("") | views.types,
        web.match("/<int:id>", 'item') | views.types_item,
    ),

    web.prefix("/commissions", name="commissions") | web.cases(
        web.match("") | views.commissions,
        web.match("/<int:id>", 'item') | views.commissions_item,
    ),
)



def get_app_cases(dynamic_app):
    if getattr(cfg, 'SERVE_STATIC', False):
        import base64, os
        from webob.exc import HTTPException
        from webob.static import FileApp

        @web.request_filter
        def cookie(env, data, nxt):
            resp = nxt(env, data)
            if resp is not None and not env.request.cookies.get('sid'):
                sid = base64.b64encode(os.urandom(20), altchars="AA").rstrip('=')
                resp.set_cookie('sid', sid)
            return resp

        @web.request_filter
        def expand_exception(env, data, nxt):
            try:
                resp = nxt(env, data)
            except HTTPException as e:
                resp = e
            if resp and hasattr(resp, 'status_code') and resp.status_code // 100 in (4, 5):
                lang = getattr(env, 'lang', 'ru')
                file_path = static.translate_path(
                        'errors/{}/{}.html'.format(lang, resp.status_code))
                if file_path and os.path.exists(file_path) and os.path.isfile(file_path):
                    return FileApp(file_path)
            return resp

        # XXX bad naming
        app_cases = expand_exception | web.cases(
          static,
          cookie | cache() | dynamic_app,
          web.match('/favicon.ico', 'favicon') | HTTPMovedPermanently('/static/favicon.ico'),
          HTTPNotFound,
        )
    else:
        app_cases = web.cases(
          cache() | dynamic_app,
          HTTPNotFound,
        )
    return app_cases



app = environment | get_app_cases(main_app)

if getattr(cfg, 'DEBUG', False):
    from iktomi import toolbar
    from iktomi.debug import debug
    if getattr(cfg, 'PRETTIFY_HTML', False):
        from .pretty_filter import pretty_html
        app = toolbar.handler(cfg) | pretty_html | debug | app
    else:
        app = toolbar.handler(cfg) | debug | app

#from pprint import pprint
#pprint(app._locations())
