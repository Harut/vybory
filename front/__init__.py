# -*- coding: utf-8 -*-
from . import cfg

#from common.app import Application
from iktomi.web import Application

from .urls import app
from .environment import db_maker, FrontEnvironment

__all__ = ['app', 'wsgi_app', 'db_maker', 'cfg']

wsgi_app = Application(app, env_class=FrontEnvironment)


