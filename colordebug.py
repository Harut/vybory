import logging

BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)

#The background is set with 40 plus the number of the color, and the foreground with 30

#These are the sequences need to get colored ouput
RESET_SEQ = "\033[0m"
COLOR_SEQ = "\033[0;%dm"
BOLD_SEQ = "\033[1m"

COLORS = {
    'WARNING': YELLOW,
    'INFO': MAGENTA,
    'DEBUG': BLUE,
    'CRITICAL': YELLOW,
    'ERROR': RED
}

class ColoredFormatter(logging.Formatter):
    FORMAT = ("[$COLOR%(levelname)s$RESET] " \
              " $COLOR%(message)s$RESET ($BOLD%(name)-6s$RESET, %(filename)s:%(lineno)d), %(asctime)s")

    def __init__(self, use_color=True):
        logging.Formatter.__init__(self, self.FORMAT)
        self.use_color = use_color

    def format(self, record):
        levelname = record.levelname

        message = self.FORMAT
        if self.use_color:
            color = COLORS[levelname]
            color = COLOR_SEQ % (30 + color) if color is not None else ''
            message = message.replace("$RESET", RESET_SEQ).replace("$BOLD", BOLD_SEQ) \
                             .replace('$COLOR', color)
        else:
            message = message.replace("$RESET", "").replace("$BOLD", "").replace('$COLOR', '')
        self._fmt = message
        return logging.Formatter.format(self, record)

