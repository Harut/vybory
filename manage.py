#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import logging
from front import cfg

from iktomi.cli import manage
from iktomi.cli.lazy import LazyCli


@LazyCli
def db_command():
    from front.environment import db_maker
    from front import models

    from iktomi.cli import sqla
    return sqla.Sqla(
        db_maker,
        metadata={'': models.metadata},
    )


@LazyCli
def err_command():
    from front.error_pages import ErrorPages
    return ErrorPages()

@LazyCli
def front_command():
    import front
    from iktomi.cli.app import App

    root = cfg.ROOT
    return App(front.wsgi_app, 
               shell_namespace={'db': front.db_maker()},
               extra_files=[os.path.join(root, 'cfg_local.py'),
                            os.path.join(root, 'front', 'cfg_local.py')])

@LazyCli
def front_fcgi_command():
    import front
    from iktomi.cli.fcgi import Flup
    return Flup(front.wsgi_app, **front.cfg.FLUP_ARGS)

def run(args=sys.argv):
    config_logging()
    manage(dict(
        # sqlalchemy session
        db = db_command,
        err = err_command,

        # dev-server
        front = front_command,

        front_fcgi = front_fcgi_command,
    ), args)


def config_logging():
    logging.basicConfig(
            format=cfg.LOG_FORMAT,
            level=logging.DEBUG)
    # http://www.sqlalchemy.org/docs/05/dbengine.html#configuring-logging
    #logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
    #logging.getLogger('sqlalchemy.pool').setLevel(logging.INFO)
    logging.getLogger('iktomi.templates').setLevel(logging.WARNING)
    logging.getLogger('iktomi.auth').setLevel(logging.WARNING)
    logging.getLogger('iktomi.web.filters').setLevel(logging.WARNING)
    logging.getLogger('iktomi.web.url_templates').setLevel(logging.WARNING)
    if getattr(cfg, 'COLOR_LOGGER', False):
        import colordebug
        color_formatter = colordebug.ColoredFormatter()
        logging.root.handlers[0].setFormatter(color_formatter)


if __name__ == '__main__':
    run()
